#!/bin/sh

# Alex Coninx
# ISIR - Sorbonne Universite / CNRS
# 7/10/2019

BASE_DIR=$PWD

INSTALL_ROOT=$HOME/src/pyfastsim-install

mkdir -p $INSTALL_ROOT

 

# 1) Install and patch fastsim
echo
echo "===================================================="
echo "===== (1/3) Patching and installing libfastsim ====="
echo "===================================================="
echo
cd $INSTALL_ROOT
git clone https://github.com/jbmouret/libfastsim.git
# We need to clone the pyfastsim repository now to get the patch
git clone https://github.com/alexendy/pyfastsim.git
cd libfastsim
# Patch libfastsim
patch -p1 < ../pyfastsim/fastsim-boost2std-fixdisplay.patch
# Build and install
python2.7 ./waf configure --prefix=./install
python2.7 ./waf build
python2.7 ./waf install
# Where we installed fastsim
FASTSIM_DIR=$INSTALL_ROOT/libfastsim/install

# 2) Install pyfastsim
echo
echo "======================================"
echo "===== (2/3) Installing pyfastsim ====="
echo "======================================"
echo
cd $INSTALL_ROOT/pyfastsim
CPPFLAGS="-I${PYBIND11_DIR}/include -I${FASTSIM_DIR}/include" LDFLAGS="-L${FASTSIM_DIR}/lib" pip3 install --user .

# 3) install fastsim_gym
echo
echo "========================================"
echo "===== (3/3) Installing fastsim_gym ====="
echo "========================================"
echo
pip3 install --user gym
cd $INSTALL_ROOT
git clone https://github.com/alexendy/fastsim_gym
cd fastsim_gym
pip3 install --user --no-deps .
echo
echo "********************************************************************************"
echo "Installation complete !"
echo "********************************************************************************"

cd $BASE_DIR

echo
echo "To test :"
echo
echo " - Go to $INSTALL_ROOT/fastsim_gym/example"
echo " - Run : python3 example_gym_fastsim.py"
echo
echo "If you see a 2D maze with a small robot moving towards a wall (and remaining stuck there) you're OK !"

