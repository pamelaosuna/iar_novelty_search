from scipy.spatial import KDTree
import random
import numpy as np
from operator import itemgetter

class NovArchive:
    """Archive used to compute novelty scores."""
    def __init__(self, lbd,lfit, k=15):
        self.all_bd=lbd
        # ADDED: keep list of fit values : 
        self.all_fit = lfit
        self.kdtree=KDTree(self.all_bd)
        self.k=k
        #print("Archive constructor. size = %d"%(len(self.all_bd)))
        
    def update(self,new_bd,new_fit):
        oldsize=len(self.all_bd)
        self.all_bd=self.all_bd + new_bd
        # ADDED: keep list of fit values : 
        self.all_fit = self.all_fit + new_fit
        self.kdtree=KDTree(self.all_bd)
        #print("Archive updated, old size = %d, new size = %d"%(oldsize,len(self.all_bd)))

    def get_nov(self,bd,fit, population=[]):
        # keep distance and fit for each individuals in population
        dist_fit_pop =[]
        for ind in population:
            dist = np.linalg.norm(np.array(bd)-np.array(ind.bd))
            dist_fit_pop.append((dist,ind.fit))
        # keep distance and fit for each individuals in archive
        darch,ind=self.kdtree.query(np.array(bd),self.k)
        dist_fit_arch = [(d,self.all_fit[i]) for (d,i) in zip(darch,ind)]
        ##test_a_suppr = [np.linalg.norm(np.array(bd)-np.array(self.all_bd[i])) for i in ind]
        # combine dist and fit for archive and population
        dist_fit=dist_fit_pop+dist_fit_arch
        dist_fit.sort(key = lambda x : x[0])

        if (dist_fit[0][0]!=0):
            print("WARNING in novelty search: the smallest distance should be 0 (distance to itself). If you see it, you probably try to get the novelty with respect to a population your indiv is not in. The novelty value is then the sum of the distance to the k+1 nearest divided by k.")
        
        # compute novelty and local_competition
        novelty = sum(x[0] for x in dist_fit[:self.k+1])/self.k # as the indiv is in the population, the first value is necessarily a 0.
        # local competition : number of neighbour with less fitness
        local_comp = sum(fit > x[1] for x in dist_fit[:self.k+1])/self.k

        return novelty,local_comp

    def size(self):
        return len(self.all_bd)
    
def updateNovelty(population, offspring, archive, k=15, add_strategy="random", _lambda=6, verbose=False):
   """Update the novelty criterion (including archive update) 

   Implementation of novelty search following (Gomes, J., Mariano, P., & Christensen, A. L. (2015, July). Devising effective novelty search algorithms: A comprehensive empirical study. In Proceedings of GECCO 2015 (pp. 943-950). ACM.).
   :param population: is the set of indiv for which novelty needs to be computed
   :param offspring: is the set of new individuals that need to be taken into account to update the archive (may be the same as population, but it may also be different as population may contain the set of parents)
   :param k: is the number of nearest neighbors taken into account
   :param add_strategy: is either "random" (a random set of indiv is added to the archive) or "novel" (only the most novel individuals are added to the archive).
   :param _lambda: is the number of individuals added to the archive for each generation
   The default values correspond to the one giving the better results in the above mentionned paper.

   The function returns the new archive
   """
   
   # Novelty scores updates
   if (archive) and (archive.size()>=k):
       if (verbose):
           print("Update Novelty. Archive size=%d"%(archive.size())) 
       for ind in population:
           # MODIFIED : 
           ind.novelty , ind.local_comp = archive.get_nov(ind.bd, ind.fit, population)
   else:
       if (verbose):
           print("Update Novelty. Initial step...") 
       for ind in population:
           ind.novelty=0.
           # ADDED : 
           ind.local_comp = 0

   if (verbose):
       print("Fitness (novelty): ",end="") 
       for ind in population:
           print("%.2f, "%(ind.novelty),end="")
       print("")
   if (len(offspring)<_lambda):
       print("ERROR: updateNovelty, lambda(%d)<offspring size (%d)"%(_lambda, len(offspring)))
       return None

   lbd=[]
   lfit=[]
   # Update of the archive
   if(add_strategy=="random"):
       # random individuals are added
       l=list(range(len(offspring)))
       random.shuffle(l)
       if (verbose):
           print("Random archive update. Adding offspring: "+str(l[:_lambda])) 
       lbd=[offspring[l[i]].bd for i in range(_lambda)]
       # ADDED : list of fit values
       lfit=[offspring[l[i]].fit for i in range(_lambda)]
   elif(add_strategy=="novel"):
       # the most novel individuals are added
       soff=sorted(offspring,lambda x:x.novelty)
       ilast=len(offspring)-_lambda
       lbd=[soff[i].bd for i in range(ilast,len(soff))]
       if (verbose):
           print("Novel archive update. Adding offspring: ")
           for offs in soff[iLast:len(soff)]:
               print("    nov="+str(offs.novelty)+" fit="+str(offs.fitness.values)+" bd="+str(offs.bd))
   else:
       print("ERROR: updateNovelty: unknown add strategy(%s), valid alternatives are \"random\" and \"novel\""%(add_strategy))
       return None
       
   # ADD fit (=-coll) in archive : 
   if(archive==None):
       archive=NovArchive(lbd,lfit,k)
   else:
       archive.update(lbd,lfit)

   return archive
